use bevy::prelude::*;
use std::collections::HashMap;

use crate::{
    schedule::UpdatePhase,
    state::{PauseEvent, SceneState},
};

#[cfg(feature = "controller_support")]
use gamepad::map_gamepads;

pub use keyboard::KeyboardControlSection;
use keyboard::{
    map_keyboard, reset_last_direction_inputs, update_directional_input_order,
    DirectionalInputOrder,
};

#[cfg(feature = "controller_support")]
mod gamepad;
mod keyboard;

#[derive(Debug, Hash, PartialEq, Eq)]
pub enum InputDevice {
    Keyboard(KeyboardControlSection),
    #[cfg(feature = "controller_support")]
    Gamepad(usize),
}

#[derive(Resource, Debug, Default)]
pub struct InputMap(pub HashMap<InputDevice, InputMapping>);

#[derive(Debug, Component)]
pub struct ControllerAssignment(pub InputDevice);

#[derive(Debug)]
pub struct InputMapping {
    /// x-axis from `-1.0` left to `1.0` right
    pub x: f32,
    /// dash button just pressed
    pub just_dashed: bool,
    /// jump button held
    pub _jump: bool,
    /// jump button just pressed
    pub just_jumped: bool,
}

impl InputMapping {
    pub fn new(x: f32, just_dashed: bool, jump: bool, just_jumped: bool) -> Self {
        Self {
            x,
            just_dashed,
            _jump: jump,
            just_jumped,
        }
    }
}

pub struct InputPlugin;

impl Plugin for InputPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<InputMap>()
            .init_resource::<DirectionalInputOrder>()
            .add_systems(
                Update,
                (update_directional_input_order, map_keyboard).in_set(UpdatePhase::UserInput),
            )
            .add_systems(OnEnter(SceneState::InGame), reset_last_direction_inputs)
            .add_systems(Update, handle_pause.run_if(in_state(SceneState::InGame)));

        #[cfg(feature = "controller_support")]
        app.add_systems(Update, map_gamepads.in_set(UpdatePhase::UserInput));
    }
}

#[cfg(feature = "controller_support")]
fn handle_pause(
    mut pause_event: EventWriter<PauseEvent>,
    gamepads: Res<Gamepads>,
    gamepad_buttons: Res<ButtonInput<GamepadButton>>,
    keyboard_input: Res<ButtonInput<KeyCode>>,
) {
    let controller_pressed_pause = gamepads.iter().any(|gamepad| {
        gamepad_buttons.just_pressed(GamepadButton {
            gamepad,
            button_type: GamepadButtonType::Start,
        })
    });

    if controller_pressed_pause || keyboard_input.just_pressed(KeyCode::Escape) {
        pause_event.send(PauseEvent);
    }
}

#[cfg(not(feature = "controller_support"))]
fn handle_pause(
    mut pause_event: EventWriter<PauseEvent>,
    keyboard_input: Res<ButtonInput<KeyCode>>,
) {
    if keyboard_input.just_pressed(KeyCode::Escape) {
        pause_event.send(PauseEvent);
    }
}
