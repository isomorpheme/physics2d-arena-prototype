mod game_hud;
mod game_over_menu;
mod main_menu;
mod pause_menu;
pub mod styles;

use bevy::prelude::*;

use game_hud::GameHudPlugin;
use game_over_menu::GameOverMenuPlugin;
use main_menu::MainMenuPlugin;
use pause_menu::PauseMenuPlugin;

use styles::{BUTTON_COLOR, HOVERED_BUTTON_COLOR, PRESSED_BUTTON_COLOR};

pub struct UiPlugin;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(MainMenuPlugin)
            .add_plugins(PauseMenuPlugin)
            .add_plugins(GameOverMenuPlugin)
            .add_plugins(GameHudPlugin)
            .add_systems(Update, handle_button_interaction_colors);
    }
}

fn clear_menu<T: Component>(mut commands: Commands, query: Query<Entity, With<T>>) {
    if let Ok(menu) = query.get_single() {
        commands.entity(menu).despawn_recursive();
    }
}

pub type ButtonQuerySelection<'a, 'b> = (&'a Interaction, &'b mut BackgroundColor);
pub type ButtonQueryFilter<T> = (Changed<Interaction>, With<T>);
fn handle_button_interaction_colors(
    mut query: Query<ButtonQuerySelection, ButtonQueryFilter<Button>>,
) {
    for (interaction, mut background_color) in query.iter_mut() {
        match *interaction {
            Interaction::Pressed => *background_color = PRESSED_BUTTON_COLOR.into(),
            Interaction::Hovered => *background_color = HOVERED_BUTTON_COLOR.into(),
            Interaction::None => *background_color = BUTTON_COLOR.into(),
        }
    }
}
