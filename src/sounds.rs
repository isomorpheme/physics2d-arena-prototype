use bevy::{audio::PlaybackMode, prelude::*};

const JUMP_SFX: &str = "sfx/jump.wav";
const DENY_SFX: &str = "sfx/deny.wav";
const DASH_SFX: &str = "sfx/dash.wav";
const HURT_SFX: &str = "sfx/hurt.wav";

#[derive(Debug, Clone, Copy)]
pub enum Sfx {
    Jump,
    Deny,
    Dash,
    Hurt,
}
impl Sfx {
    fn path(&self) -> &'static str {
        match self {
            Sfx::Jump => JUMP_SFX,
            Sfx::Deny => DENY_SFX,
            Sfx::Dash => DASH_SFX,
            Sfx::Hurt => HURT_SFX,
        }
    }
}

#[derive(Event)]
pub struct SfxEvent(pub Sfx);

pub struct SfxPlugin;

impl Plugin for SfxPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<SfxEvent>()
            .add_systems(Update, handle_sfx_event);
    }
}

fn handle_sfx_event(
    mut commands: Commands,
    mut sfx_events: EventReader<SfxEvent>,
    asset_server: Res<AssetServer>,
) {
    for &SfxEvent(sfx) in sfx_events.read() {
        commands.spawn(AudioBundle {
            source: asset_server.load(sfx.path()),
            settings: PlaybackSettings {
                mode: PlaybackMode::Despawn,
                ..default()
            },
        });
    }
}
