use bevy::prelude::*;

use crate::{
    players::{
        health::{DamageEvent, Health},
        Player, Slot,
    },
    state::SceneState,
};

use super::{
    clear_menu,
    styles::{game_hud_container_style, health_bar_container_style, HEALTH_BAR_BACKGROUND_COLOR},
};

#[derive(Component)]
struct GameHud;

#[derive(Component)]
struct HealthBar;

#[derive(Resource, Default)]
struct HealthIcons {
    full: Handle<Image>,
    empty: Handle<Image>,
}
const FULL_HEART_ICON_PATH: &str = "icons/full.png";
const EMPTY_HEART_ICON_PATH: &str = "icons/empty.png";

pub struct GameHudPlugin;

impl Plugin for GameHudPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<HealthIcons>()
            .add_systems(Startup, load_health_icons)
            .add_systems(OnEnter(SceneState::InGame), draw_main_menu)
            .add_systems(OnExit(SceneState::GameOver), clear_menu::<GameHud>)
            .add_systems(Update, update_hud.run_if(in_state(SceneState::InGame)))
            .add_systems(Update, update_hud.run_if(in_state(SceneState::GameOver)));
    }
}

fn load_health_icons(mut icons: ResMut<HealthIcons>, asset_loader: Res<AssetServer>) {
    icons.full = asset_loader.load(FULL_HEART_ICON_PATH);
    icons.empty = asset_loader.load(EMPTY_HEART_ICON_PATH);
}

fn draw_main_menu(mut commands: Commands, icons: Res<HealthIcons>) {
    commands
        .spawn((
            NodeBundle {
                style: game_hud_container_style(),
                ..default()
            },
            GameHud,
        ))
        .with_children(|parent| {
            // Player One Health Bar
            parent
                .spawn((
                    NodeBundle {
                        style: health_bar_container_style(),
                        background_color: HEALTH_BAR_BACKGROUND_COLOR.into(),
                        ..default()
                    },
                    HealthBar,
                    Slot::One,
                ))
                .with_children(|parent| {
                    parent.spawn((health_icon(&icons.full), Health(1)));
                    parent.spawn((health_icon(&icons.full), Health(2)));
                    parent.spawn((health_icon(&icons.full), Health(3)));
                });

            // Player Two Health Bar            parent
            parent
                .spawn((
                    NodeBundle {
                        style: health_bar_container_style(),
                        background_color: HEALTH_BAR_BACKGROUND_COLOR.into(),
                        ..default()
                    },
                    HealthBar,
                    Slot::Two,
                ))
                .with_children(|parent| {
                    parent.spawn((health_icon(&icons.full), Health(1)));
                    parent.spawn((health_icon(&icons.full), Health(2)));
                    parent.spawn((health_icon(&icons.full), Health(3)));
                });
        });
}

fn health_icon(icon: &Handle<Image>) -> ImageBundle {
    ImageBundle {
        image: icon.clone().into(),
        ..default()
    }
}

fn update_hud(
    mut damage_events: EventReader<DamageEvent>,
    players: Query<(&Slot, &Health), With<Player>>,
    health_bars: Query<(&Slot, &Children), With<HealthBar>>,
    mut health_icons: Query<(&Health, &mut UiImage)>,
    icons: Res<HealthIcons>,
) {
    for event in damage_events.read() {
        let (player_slot, player_health) = players.get(event.target).unwrap();
        if let Some((_, children)) = health_bars
            .iter()
            .find(|(bar_slot, _)| &player_slot == bar_slot)
        {
            for &child in children.iter() {
                if let Ok((icon_health, mut image)) = health_icons.get_mut(child) {
                    if player_health >= icon_health {
                        image.texture = icons.full.clone();
                    } else {
                        image.texture = icons.empty.clone();
                    }
                }
            }
        }
    }
}
