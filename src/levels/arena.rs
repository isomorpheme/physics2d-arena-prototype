use bevy::{
    prelude::*,
    sprite::{MaterialMesh2dBundle, Mesh2dHandle},
};
use bevy_rapier2d::prelude::*;

use super::{Level, LevelSelect, SpawnLocation};

// Camera Scale of 2x means 1px = 2 game units
// window has a 1280x720px size
// therefore 2560x1440 game units
const CAMERA_SCALE: f32 = 2.0;

const GROUND_COLOR: Color = Color::BEIGE;
const FLOOR_WIDTH: f32 = 2560.0;
const FLOOR_HEIGHT: f32 = 100.0;
const FLOOR_Y_OFFSET: f32 = -670.0;

const SPAWN_PLATFORM_WIDTH: f32 = 500.0;
const SPAWN_PLATFORM_HEIGHT: f32 = 100.0;
const SPAWN_PLATFORM_Y_OFFSET: f32 = -100.0;

const SPAWNS: [Vec3; 2] = [Vec3::new(-50.0, 0.0, 0.0), Vec3::new(50.0, 0.0, 0.0)];

const WALL_WIDTH: f32 = 50.0;
const WALL_HEIGHT: f32 = 1232.0;
const WALL_X_OFFSET: f32 = 1255.0;

pub struct ArenaPlugin;

impl Plugin for ArenaPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(OnEnter(LevelSelect::Arena), spawn_world)
            .add_systems(OnExit(LevelSelect::Arena), despawn_world);
    }
}

fn spawn_world(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut projections: Query<&mut OrthographicProjection>,
) {
    // Update camera scale
    for mut projection in &mut projections {
        projection.scale = CAMERA_SCALE;
    }

    // Spawn level geometry
    let spawn_platform_mesh =
        Mesh2dHandle(meshes.add(Rectangle::new(SPAWN_PLATFORM_WIDTH, SPAWN_PLATFORM_HEIGHT)));
    let material = materials.add(GROUND_COLOR);

    commands
        .spawn((
            Level,
            SpatialBundle {
                transform: Transform::from_translation(Vec3::ZERO),
                ..default()
            },
        ))
        .with_children(|parent| {
            parent
                .spawn(MaterialMesh2dBundle {
                    mesh: spawn_platform_mesh,
                    material: material.clone(),
                    transform: Transform::from_xyz(0.0, SPAWN_PLATFORM_Y_OFFSET, 0.0),
                    ..default()
                })
                .insert(RigidBody::Fixed)
                .insert(Collider::cuboid(
                    SPAWN_PLATFORM_WIDTH / 2.0,
                    SPAWN_PLATFORM_HEIGHT / 2.0,
                ));

            let floor_mesh = Mesh2dHandle(meshes.add(Rectangle::new(FLOOR_WIDTH, FLOOR_HEIGHT)));

            // Floor
            parent
                .spawn(MaterialMesh2dBundle {
                    mesh: floor_mesh.clone(),
                    material: material.clone(),
                    transform: Transform::from_xyz(0.0, FLOOR_Y_OFFSET, 0.0),
                    ..default()
                })
                .insert(RigidBody::Fixed)
                .insert(Collider::cuboid(FLOOR_WIDTH / 2.0, FLOOR_HEIGHT / 2.0));

            // Ceiling
            parent
                .spawn(MaterialMesh2dBundle {
                    mesh: floor_mesh.clone(),
                    material: material.clone(),
                    transform: Transform::from_xyz(0.0, -FLOOR_Y_OFFSET, 0.0),
                    ..default()
                })
                .insert(RigidBody::Fixed)
                .insert(Collider::cuboid(FLOOR_WIDTH / 2.0, FLOOR_HEIGHT / 2.0));

            let wall_mesh = Mesh2dHandle(meshes.add(Rectangle::new(WALL_WIDTH, WALL_HEIGHT)));
            // Left Wall
            parent
                .spawn(MaterialMesh2dBundle {
                    mesh: wall_mesh.clone(),
                    material: material.clone(),
                    transform: Transform::from_xyz(-WALL_X_OFFSET, 0.0, 0.0),
                    ..default()
                })
                .insert(RigidBody::Fixed)
                .insert(Collider::cuboid(WALL_WIDTH / 2.0, WALL_HEIGHT / 2.0));
            // Right Wall
            parent
                .spawn(MaterialMesh2dBundle {
                    mesh: wall_mesh,
                    material: material.clone(),
                    transform: Transform::from_xyz(WALL_X_OFFSET, 0.0, 0.0),
                    ..default()
                })
                .insert(RigidBody::Fixed)
                .insert(Collider::cuboid(WALL_WIDTH / 2.0, WALL_HEIGHT / 2.0));

            // Spawn Locations
            for spawn_position in SPAWNS {
                parent.spawn(SpawnLocation::new(spawn_position));
            }
        });
}

fn despawn_world(mut commands: Commands, query: Query<Entity, With<Level>>) {
    if let Ok(level) = query.get_single() {
        commands.entity(level).despawn_recursive();
    }
}
