use bevy::prelude::*;

use crate::state::{GameState, SceneState};

#[derive(SystemSet, Debug, Hash, PartialEq, Eq, Clone)]

pub enum UpdatePhase {
    UserInput,
    Movement,
    CollisionDetection,
    EntityUpdates,
}

pub struct SchedulePlugin;

impl Plugin for SchedulePlugin {
    fn build(&self, app: &mut App) {
        app.configure_sets(
            Update,
            (
                UpdatePhase::UserInput,
                UpdatePhase::Movement,
                UpdatePhase::CollisionDetection,
                UpdatePhase::EntityUpdates,
            )
                .chain()
                .run_if(in_state(SceneState::InGame))
                .run_if(in_state(GameState::Running)),
        );
    }
}
