use bevy::prelude::*;

use crate::schedule::UpdatePhase;

use super::{health::DamageEvent, ColorSet, Player};

const BLINK_INTERVAL: f32 = 0.2;
const INVINCIBILITY_DURATION: f32 = 1.5;

#[derive(Component, Debug, Deref, DerefMut)]
pub struct Invincibility(pub Timer);

pub struct InvincibilityPlugin;

impl Plugin for InvincibilityPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            Update,
            (
                add_invincibility_to_player,
                remove_expired_invincibility,
                blink_invincibility,
                tick_invincibility,
            )
                .chain()
                .in_set(UpdatePhase::EntityUpdates),
        );
    }
}

fn add_invincibility_to_player(
    mut damage_events: EventReader<DamageEvent>,
    mut commands: Commands,
) {
    for event in damage_events.read() {
        commands
            .get_entity(event.target)
            .unwrap()
            .insert(Invincibility(Timer::from_seconds(
                INVINCIBILITY_DURATION,
                TimerMode::Once,
            )));
    }
}

fn blink_invincibility(
    mut query: Query<(&mut Handle<ColorMaterial>, &Invincibility, &ColorSet), With<Player>>,
) {
    for (mut color_handle, invincibility, colors) in query.iter_mut() {
        // we'll blink the first 0.2 seconds, be normal for 0.2s and repeat
        let control = (invincibility.remaining_secs() % (BLINK_INTERVAL * 2.0)) / BLINK_INTERVAL;

        if control <= 1.0 {
            *color_handle = colors.opague.clone();
        } else {
            *color_handle = colors.default.clone();
        }
    }
}

fn remove_expired_invincibility(
    mut commands: Commands,
    mut query: Query<(
        Entity,
        &mut Handle<ColorMaterial>,
        &ColorSet,
        &Invincibility,
    )>,
) {
    for (entity, mut color_handle, colors, timer) in &mut query {
        if timer.finished() {
            *color_handle = colors.default.clone();
            commands
                .get_entity(entity)
                .unwrap()
                .remove::<Invincibility>();
        }
    }
}

fn tick_invincibility(mut query: Query<&mut Invincibility>, delta: Res<Time>) {
    for mut timer in &mut query {
        timer.tick(delta.delta());
    }
}
