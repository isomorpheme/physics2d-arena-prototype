use bevy::prelude::*;
use bevy_rapier2d::control::KinematicCharacterControllerOutput;
use std::collections::HashSet;

use super::{
    health::DamageEvent,
    invincibility::Invincibility,
    movement::{state::MovementState, Speed},
    Player,
};
use crate::schedule::UpdatePhase;

const COLLISION_DAMAGE: i16 = 1;

pub struct CollisionPlugin;

impl Plugin for CollisionPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, collide.in_set(UpdatePhase::CollisionDetection));
    }
}

type CollisionFilter = (With<Player>, Without<Invincibility>);
fn collide(
    mut query: Query<
        (
            Entity,
            &mut MovementState,
            &Speed,
            &KinematicCharacterControllerOutput,
        ),
        CollisionFilter,
    >,
    mut damage_events: EventWriter<DamageEvent>,
) {
    let characters = query
        .iter()
        .map(|(entity, _, _, _)| entity)
        .collect::<HashSet<Entity>>();

    for (_entity, mut movement_state, speed, output) in query.iter_mut() {
        for collision in output.collisions.iter() {
            if characters.contains(&collision.entity) {
                // TODO: Hit from above

                // dash hit
                if speed.linvel.x.abs() > 1000.0 {
                    *movement_state = MovementState::Jumping;
                    damage_events.send(DamageEvent {
                        target: collision.entity,
                        amount: COLLISION_DAMAGE,
                    });
                }
            }
        }
    }
}
