use bevy::{prelude::*, time::Stopwatch};

pub const DASH_DURATION: f32 = 0.3;
pub const DASH_COOLDOWN: f32 = 1.0;

#[derive(Component, Debug, Clone, Copy, Default, Eq, PartialEq, Hash)]
pub enum MovementState {
    #[default]
    Idle,
    Walking,
    Jumping,
    Dashing,
}

#[derive(Bundle, Debug)]
pub struct MovementStateBundle {
    state: MovementState,
    dash_timer: DashTimer,
    dash_cooldown_timer: DashCooldownTimer,
    air_time: AirTime,
}

/// The Timer that controls the physics suspension for the Dash itself, not to be
/// confused with the DashCooldownTimer
#[derive(Component, Debug, Deref, DerefMut, Default)]
pub struct DashTimer(Timer);

#[derive(Component, Debug, Deref, DerefMut, Default)]
pub struct DashCooldownTimer(Timer);

#[derive(Component, Default, Debug, Deref, DerefMut)]
pub struct AirTime(Stopwatch);

impl Default for MovementStateBundle {
    fn default() -> Self {
        let mut air_time = AirTime(Stopwatch::new());
        air_time.0.pause();
        Self {
            state: MovementState::default(),
            dash_timer: DashTimer(Timer::from_seconds(DASH_DURATION, TimerMode::Once)),
            dash_cooldown_timer: DashCooldownTimer(Timer::from_seconds(
                DASH_COOLDOWN,
                TimerMode::Once,
            )),
            air_time,
        }
    }
}

pub fn tick_dash_timers(
    mut query: Query<(&mut DashTimer, &mut DashCooldownTimer)>,
    time: Res<Time>,
) {
    for (mut dash_timer, mut dash_cooldown_timer) in &mut query {
        dash_timer.tick(time.delta());
        dash_cooldown_timer.tick(time.delta());
    }
}

pub fn tick_air_time(mut query: Query<(&mut AirTime, &MovementState)>, time: Res<Time>) {
    for (mut air_time, state) in &mut query {
        if state == &MovementState::Jumping {
            air_time.tick(time.delta());
        }
    }
}
