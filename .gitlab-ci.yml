stages:
  - prepare
  - test
  - build
  - deploy

# Make sure CI fails on all warnings, including Clippy lints
variables:
  RUSTFLAGS: "-Dwarnings"
  ITCH_USER: good-praxis
  ITCH_PROJECT: 2d-platformer-arena
  CARGO_HOME: ${CI_PROJECT_DIR}/.cargo

.rust:
  image: $CI_REGISTRY_IMAGE/buildimage:latest
  cache:
    - &cargo_cache
      key:
        files:
          - Cargo.lock
        prefix: $CI_JOB_STAGE
      paths:
        - .cargo/registry/index
        - .cargo/registry/cache 
    - &rust_cache
      key: $CI_JOB_STAGE
      paths:
        - target/debug/deps
        - target/debug/incremental
        - target/debug/build

meta-build-image-latest:
  image: docker:24.0.7
  tags:
    - buildimage
  services:
    - name: docker:24.0.7-dind
  stage: prepare
  script:
    - echo $CI_REGISTRY_PASSWORD | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
    - cd .meta/latest
    - docker build -t $CI_REGISTRY_IMAGE/buildimage:latest .
    - docker push $CI_REGISTRY_IMAGE/buildimage:latest
  only:
    refs:
      - main
    changes:
      - .meta/latest/Dockerfile

meta-build-image-nightly:  
  image: docker:24.0.7
  tags:
    - buildimage
  services:
    - name: docker:24.0.7-dind
  stage: prepare
  script:
    - echo $CI_REGISTRY_PASSWORD | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
    - cd .meta/nightly
    - docker build -t $CI_REGISTRY_IMAGE/nightlybuildimage:latest .
    - docker push $CI_REGISTRY_IMAGE/nightlybuildimage:latest
  only:
    refs:
      - main
    changes:
      - .meta/nightly/Dockerfile

rustfmt:
  stage: test
  allow_failure: true
  extends: .rust
  script:
    - rustup component add rustfmt
    - cargo fmt -- --check

clippy_check:
  stage: test
  extends: .rust
  script:
    - rustup component add clippy
    - cargo clippy --all-targets --all-features

rust-latest:
  stage: test
  extends: .rust
  script:
    - cargo build --verbose
    - cargo test --verbose

rust-nightly:
  stage: test
  extends: .rust
  image: $CI_REGISTRY_IMAGE/nightlybuildimage:latest
  script:
    - cargo build --verbose
    - cargo test --verbose
  allow_failure: true

build-release-linux:
  stage: build
  extends: .rust
  script:
    - cargo build --verbose --release --target x86_64-unknown-linux-gnu
    - mkdir linux
    - mv ./target/x86_64-unknown-linux-gnu/release/physics2d-arena-prototype ./linux/
    - mv ./assets ./linux/
  artifacts:
    paths:
      - linux
  cache:
    - *cargo_cache
    - key:
        files:
          - Cargo.lock
        prefix: $CI_JOB_NAME
      paths:
        - target/x86_64-unknown-linux-gnu/release/deps
        - target/x86_64-unknown-linux-gnu/release/incremental
        - target/x86_64-unknown-linux-gnu/release/build
  only:
    - deploy

build-release-windows:
  stage: build
  extends: .rust
  script:
    - rustup target add x86_64-pc-windows-gnu
    - cargo build --verbose --release --target x86_64-pc-windows-gnu
    - mkdir win
    - mv ./target/x86_64-pc-windows-gnu/release/physics2d-arena-prototype.exe ./win/
    - mv ./assets ./win/
  artifacts:
    paths:
      - win
  cache:
    - *cargo_cache
    - key:
        files:
          - Cargo.lock
        prefix: $CI_JOB_NAME
      paths:
        - target/x86_64-pc-windows-gnu/release/deps
        - target/x86_64-pc-windows-gnu/release/incremental
        - target/x86_64-pc-windows-gnu/release/build
  only:
    - deploy

build-release-wasm:
  stage: build
  extends: .rust
  script:
    - rustup target add wasm32-unknown-unknown
    - cargo install wasm-bindgen-cli 
    - cargo build --verbose --release --target wasm32-unknown-unknown
    - .cargo/bin/wasm-bindgen --no-typescript --target web --out-dir ./out/ --out-name "physics2d-arena-prototype" ./target/wasm32-unknown-unknown/release/physics2d-arena-prototype.wasm 
    - mkdir wasm
    - mv ./out/* ./wasm/
    - mv ./assets/static/index.html ./wasm/
    - mv ./assets ./wasm/
    - zip -r wasm.zip ./wasm/*
  artifacts:
    paths:
      - wasm.zip
  cache:
    - *cargo_cache
    - key:
        files:
          - Cargo.lock
        prefix: $CI_JOB_NAME
      paths:
        - target/wasm32-unknown-unknown/release/deps
        - target/wasm32-unknown-unknown/release/incremental
        - target/wasm32-unknown-unknown/release/build
  only:
    - deploy
  allow_failure: true

itch:
  image: dosowisko/butler
  stage: deploy
  script:
    - VERSION=$CI_COMMIT_SHORT_SHA
    - butler push "./linux" "${ITCH_USER}/${ITCH_PROJECT}:linux" --userversion $VERSION
    - butler push "./win" "${ITCH_USER}/${ITCH_PROJECT}:windows" --userversion $VERSION
    - butler push "./wasm.zip" "${ITCH_USER}/${ITCH_PROJECT}:html5" --userversion $VERSION
  only:
    - deploy 


