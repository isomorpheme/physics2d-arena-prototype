## Constraints

This game is designed with the learning objective constraints from the supervision agreement, namely:

- ~~composing a Game Design Document (GDD), with a technical focus~~
  -  This very document ✔️
- using an Entity Component System (ECS)
  - Bevy is based on an ECS system
  - DONE gain an understanding of ECS https://docs.rs/bevy_ecs/latest/bevy_ecs/
- using Physics, Sound, Particle and other subsystems
  - Physics: https://github.com/dimforge/bevy_rapier
    - https://taintedcoders.com/bevy/rapier/
    - DONE implemented physics systems
  - Sound: https://github.com/bevyengine/bevy/tree/v0.12.1/examples/audio
    - DONE implement some sfx system
  - Particles: https://github.com/djeedai/bevy_hanabi
    - DONE implement particle effects somewhere
    - instead of hanabi I opted for https://github.com/abnormalbrain/bevy_particle_systems since it compiles to wasm
- using Engine and component lifecycles and APIs
  - Engine lifecycle https://docs.rs/bevy/latest/bevy/app/struct.Main.html
    - DONE make use of different lifecycle schedules
  - Schedules https://bevy-cheatbook.github.io/programming/intro-code.html#schedules
    - DONE use custom schedules to have more control
- player interfaces
  - physical:
    - DONE Controller support
  - UI:
    - DONE menus & ui system
- game state handling
- game design principles, game mechanics
- "AI" and pathfinding
  - Probably no time to flesh this part out with the chosen prototype.

## Prototyping

### Prototype 1: glTF Model & Animation

https://codeberg.org/good_praxis/bevy_gltf_model_animation

Questions to answer: How do I play animations on glTF models?

Results: Rather simple, the SceneBundle spawns an AnimationPlayer, we listen for it to be added (or for another appropriate state through scheduling) and then query for the AnimationPlayer to play a given loaded animation

### Prototype 2: Physics Engine 3D

https://codeberg.org/good_praxis/bevy_rapier3d_physics_prototype

Questions to answer:

How do I fit colliders with glTF models?

Furthermore: Character Controllers

Results:

the glTF models (or rather their meshes) chosen aren't compatible with rapier's ComputedColliders api

While there is a third-party dependency published on github (https://github.com/Defernus/bevy_gltf_collider) that one only supports TriMesh computation and still requires manual fitting in Blender

This does make sense, 3D collision fit to models isn't trivial, as we learn from early 3D games

Due to the early roadblocks in this prototype, character controllers hadn't seen application (albeit they're more easy to use than the ComputedColliders API)

**As a result of this prototype I'm scrapping the idea of making this project 3D. The main disadvantages are the time investment of fitting custom colliders to the CC0 low-poly models. A 2D game should see a quicker iteration process regardless**

### Prototype 3: 2D Platformer Versus

This is the first game-play focused prototype,

#### Specifications

Two CharacterControllers controlled via Keyboard and Gamepad

Characters of circa 100 units of height, 50 unit width with colliders on all four edges

Top collider is to detect being jumped on

Side colliders are to detect being dashed against

Bottom collider is for floors and jumping-on other characters

Movement option:

- A/D or controller x-axis: walking left and right or adjusting in air
- Space or X (controller): Jumping, double-jumping in air
- Q/E or R1/L1: Dashing left/right with cooldown

Characters have maybe 3 lives

## Design

Locking down on the outcome of prototype 3, current and future mechanics are highlighted with either `DONE` or `TODO`. Some of them are unrealistic in their listing to be implemented - there simply isn't enough time for this project to implement every desired feature properly, the mechanics that are defacto ruled out for implementation in the scope of this project are marked with `LATER`

### Movement

The movement system is broadly inspired by Celeste, the resemblance would be clearer with more time in the oven, as certain implemented systems are still pretty flat.

#### Jumping

 - DONE On descent, gravity is doubled, this leads to a quicker fall down
 - DONE Air maneuverability is reduced, making jumping a higher risk
 - TODO hook into the "air-time" timer to allow for a coyote jump
 - LATER A double-jump system might make jumps worth it more, and allows for more interesting ddging behaviour
 - TODO longer jump peak with held jump
 - TODO shorter jump if jump button is released early
 - DONE a small particle effect communicates landing/regaining "grounded" status
 - DONE sfx on jump
 - DONE sfx on denied jump

#### Dashing

- DONE Dashes have a cool down of <1 sec
- DONE During the active phase of a dash, gravity is suspended
- DONE You can dash upwards
- LATER You can dash in any discreet direction via controller axis/mouse direction vector
  - This does introduce a clear bias for gamepads though, as the axis is more versatile than having to aim with the mouse
- LATER You can dash downwards, kind of like a stomp
- DONE Dashing into the other player damages them
- TODO You can cancel a dash early
- DONE sfx on dash
- DONE sfx on denied dash

#### Walking

- DONE slide along ground surfaces with increased manueverability
- TODO faster recovery for dashes while on ground?

### Game Loop

As an arena game, the goal is it to defeat the other player(s). For this purpose there are two immediate game mode possibilities: A life-based mode and a time-limited points based mode.

The prototype implements a static lives based system (3 lives).

- The goal is to deplete the other player's lives / accumulate the most points during the round time
- The prototype is further limited to two players.

Both live and player count, game mode could easily be extended by extending their already designed to be dynamic systems.

- Preset modes of specific player Health values
- Dynamically adding UI components based on the players Health value
- For a new time based mode, add a score component, switch out Health and Score components as needed, maybe while switching to the InGame state

### Ui

#### Start Menu

![Start Menu](GDD/start_menu.png)

  - DONE add stage select:

  - ![Start Menu with Level Select](GDD/start_menu_with_level_select.png)
  
  - TODO add controller assignment:
  
  - ![Start Menu with Controller Assignment](GDD/controller.png)

#### Pause Menu

![Pause Menu](GDD/pause.png)

#### Game HUD

![Game HUD](GDD/hud.png)

#### Game Over Screen

![Game Over Menu](GDD/gameover.png)

#### Options

TODO add an options menu to allow dynamically assigning inputs to players

LATER make UI controller navigatable

-> Assign something like a tab index to buttons and make them highlight when in focus

### Audio

#### Sfx

To generate the sound effects used, I made use of https://sfbgames.itch.io/chiptone -- the sound files are CC0

The SFX system uses an event channel to trigger the effects. They didn't need to be locational and there was no need to have blocking sound files, such as you'd want with e.g. dialogue -  we can play multiple sfx at once without worry - so we simply spawn a new audio player for each Sfx event

LATER the Sfx Events are too tightly coupled with the movement system. Removing it would currently lead to a failed compile -- how might I be able to make it fully modular?

Maybe defining the events in the plugin that emits them rather than the plugin that consumes them is one solution

Current SFX:

- Jump
- Dash
- Hurt
  - On taking damage
- Deny
  - Played when an action failed (i.e. can't jump, can't dash)

### Input System

The game supports both keyboard input and controller input

As of Apr 18th, 2024 the game will pick the keyboard as the first player and the first available controller as the second one

The game will not crash if there isn't enough input devices present, instead the player has a `ControllerAssignment` component

LATER tight coupling again, probably fine in this case

`ControllerAssignment` allows for choice of keyboard or any connected controller, player one is set to keyboard, player two to `gamepad(0)`

TODO add a second keyboard mapping to allow inputs without controller present

TODO dynamically assign controller mappings instead of hardcoding them

### Online Functionality

While it was previously planned to implement networking for this game -- rapiers [enhanced determinism](https://rapier.rs/docs/user_guides/rust/determinism/) had already been turned on -- due to time constraints this will have to be scrapped

The following records the research findings:

While an authoritative server architecture would've been preferred (due to the physics simulations involved), a p2p connection would have been preferred from the developmental overhead

Ideally inputs would be transmitted in unreliable signals, while position data of the controlled player would be transmitted reliably, this should work fine on low latency. The authoritative host would allow a better experience in high latency environments

I'd have choosen to use https://github.com/johanhelsing/matchbox for this, https://github.com/johanhelsing/matchbox/tree/main/bevy_matchbox

Of course a signaling server would still be required, but again the overhead would be smaller than having a separate server binary that runs the physics simulation on it's own then transmits to and polls from the players

### Other features

#### WASM compatibility

Libraries and functionality was chosen to ensure a potential deployment to web. There is a CI job ensuring it compiles for a wasm target
